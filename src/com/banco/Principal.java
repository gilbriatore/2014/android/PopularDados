package com.banco;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Principal extends Activity {

	SQLiteDatabase banco;

	private String[][] famosos = { { "Chico", "Anísio", "chicoanisio.jpg" },
			{ "Fábio", "Junior", "fabiojunior.jpg" },
			{ "Jim", "Carrey", "jimcarrey.jpg" },
			{ "Jô", "Soares", "josoares.jpg" },
			{ "Michael", "Jackson", "michael.jpg" },
			{ "Barack", "Obama", "obama.jpg" },
			{ "Antonio", "Palocci", "palocci.jpg" },
			{ "Vladimir", "Putin", "putin.jpg" },
			{ "Roberto", "Carlos", "roberto.jpg" },
			{ "Ronaldinho", "(Gaúcho)", "ronaldinho.jpg" },
			{ "Ronaldo", "(Fenômeno)", "ronaldo.jpg" },
			{ "José", "Serra", "serra.jpg" },
			{ "Will", "Smith", "willsmith.jpg" },
			{ "Xuxa", "Meneguel", "xuxa.jpg" } };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);

		banco = openOrCreateDatabase("banco.db", Context.MODE_PRIVATE, null);
		banco.execSQL("CREATE TABLE IF NOT EXISTS pessoa (id integer primary key autoincrement, "
				+ "nome varchar(100), sobrenome varchar(100), foto blob);");

		for (String[] famoso : famosos) {
			try {
				InputStream is = getAssets().open(famoso[2]);
				byte[] buffer = new byte[is.available()];
				is.read(buffer);

				ContentValues valores = new ContentValues(1);
				valores.put("nome", famoso[0]);
				valores.put("sobrenome", famoso[1]);
				valores.put("foto", buffer);
				banco.insert("pessoa", null, valores);

			} catch (IOException e) {
				Log.e("Principal", "Erro de leitura", e);
			}
		}
	}

	public void mostrar(View v) {
		
		EditText txtID = (EditText) findViewById(R.id.editText1);

		TextView txtNome = (TextView) findViewById(R.id.textView4);
		TextView txtSobrenome = (TextView) findViewById(R.id.textView5);
		ImageView foto = (ImageView) findViewById(R.id.imageView1);
		
		String sql = "select * from pessoa p where p.id = ? ;";
		String[] params = {"" + txtID.getText().toString()};
		
		Cursor resultado = banco.rawQuery(sql, params);
		resultado.moveToFirst();
		
		txtNome.setText(resultado.getString(resultado.getColumnIndex("nome")));
		txtSobrenome.setText(resultado.getString(resultado.getColumnIndex("sobrenome")));
		
		byte[] blob = resultado.getBlob(resultado.getColumnIndex("foto"));
		Bitmap bmp = BitmapFactory.decodeByteArray(blob, 0, blob.length);
		foto.setImageBitmap(bmp);

	}
	
	@Override
	protected void onStop() {
		super.onStop();
		banco.close();
	}

}
